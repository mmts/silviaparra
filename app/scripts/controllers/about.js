'use strict';

/**
 * @ngdoc function
 * @name silviaParraApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the silviaParraApp
 */
angular.module('silviaParraApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
