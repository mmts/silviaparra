'use strict';

/**
 * @ngdoc function
 * @name silviaParraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the silviaParraApp
 */
angular.module('silviaParraApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
